const path = require("path");
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const isProduction = process.env.NODE_ENV == "production";

const config = {

    context: __dirname, // to automatically find tsconfig.json
    mode: isProduction ? "production" : "development",
    devtool: "source-map",
    optimization: {
        usedExports: true
    },

    module: {
        rules: [
            { test: /\.tsx?$/, loader: "ts-loader" },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", ".txt"],
        fallback: { "path": false, "fs": false }
    },
    plugins: [
        new HtmlWebpackPlugin({
            //template: "./src/index.html"
        }),
        new ForkTsCheckerWebpackPlugin(),
        new ESLintPlugin({
            extensions: [".tsx", ".ts", ".js"],
            exclude: "node_modules",
        }),
    ],
};

const app = {
    ...config,
    entry: {
        main: "./src/Main.bs.js",
    },
    output: {
        filename: "[name].js",//"[name].[contenthash].js",
        path: path.resolve(__dirname, "dist"),
    },
    devServer: {
        static: "./dist"
    },
    dependencies: ["beat-detector-worklet"]
};

const worklet = {
    ...config,
    mode: "production",
    name: "beat-detector-worklet",
    target: "webworker",
    entry: "./src/worklets/beat-detector-worklet.js",
    output: {
        filename: "beat-detector-worklet.min.js",
        path: path.resolve("./src/worklets"),
    },
    performance: {
        hints: false
    }
};

module.exports = () => {
    return [
        app,
        worklet
    ];
};