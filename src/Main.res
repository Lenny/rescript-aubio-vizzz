type beatInfo = {
  bpm: float,
  confidence: float,
}

@module("./beatdetection")
external setupBeatDetection: (beatInfo => unit) => Promise.t<unit> = "setupBeatDetection"
let main = {
  Js.log("initializing beat detection...")
  setupBeatDetection(info => {
    Js.log(info.bpm)
  })
}
